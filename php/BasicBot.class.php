<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

class BasicBot {
	protected $sock, $debug;


	protected $_speedStrategy;
	protected $_carName = 'ArturamVajagMasinu';
	protected $_messages = array();

	function __construct($host, $port, $botname, $botkey, $debug = FALSE) {
		$this->debug = false;
		$this->connect($host, $port, $botkey);
		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}
	
	public function run() {

		require_once ('TrackAnalyzer.php');

		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {
				case 'join':
				case 'yourCar':
					$this->write_msg('ping', null);
					break;
				case 'gameInit':
				    $track = $msg['data']['race']['track'];
					$analyzer = new TrackAnalyzer($track);
					$analyzer->analyze();
					break;
				case 'carPositions':
					$lane = $this->getLane($msg['data'], $this->_carName);
					$piece = $this->getPiece($msg['data'], $this->_carName);
					$this->logMessage($msg);
					$this->write_msg('throttle', $analyzer->getSpeed($lane, $piece));
					break;
				case 'gameStart':
				case 'crash':
				case 'spawn':
				case 'lapFinished':
				case 'dnf':
					break;
				case 'finish':
					var_export($this->_messages);
				default:
					$this->write_msg('ping', null);
			}
		}
	}

	public function logMessage($msg)
	{
		$this->_messages[] = $msg;
	}


	public function getLane($data, $carName)
	{

		foreach ($data as $car)
		{
			if ($car['id']['name'] == $carName)
			{
				return $car['piecePosition']['lane']['endLaneIndex'];
			}
		}

		return 0;
	}

	public function getPiece($data, $carName)
	{
		foreach ($data as $car)
		{
			if ($car->id['name'] == $this->$carName)
			{
				return $car['piecePosition']['pieceIndex'];
			}
		}
		return 0;
	}

	public function getSpeedStrategy($track)
	{
		require_once ('TrackAnalyzer.php');
		$analyzer = new TrackAnalyzer($track);
		$analyzer->analyze();
		return $analyzer->getSpeedStrategy(0);
	}
}

