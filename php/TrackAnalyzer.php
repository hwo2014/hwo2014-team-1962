<?php
/**
 * Created by PhpStorm.
 * User: andris
 * Date: 16/04/14
 * Time: 09:38
 */

class TrackAnalyzer {

	private $_trackData = null;
	private $_id = "";
	private $_lanes = array();
	private $_trackPieces = array();
	private $_speedStrategy = array();

	const SPEED_BASE = 1.82;

	public function __construct($trackData)
	{
		$this->_trackData = (object) $trackData;

	 	$this->_id = $trackData->id;
	}

	public function analyze()
	{
		// Get lane information
		foreach ($this->_trackData->lanes as $lane)
		{
			$lane = (object) $lane;
			$this->_lanes[$lane->index] = $lane->distanceFromCenter;
		}

		foreach ($this->_trackData->pieces as $piece)
		{
			$piece = (object) $piece;
			$pieceData = new StdClass;
			if (!empty($piece->length))
			{
				$pieceData->type = 'straight';
			}
			else
			{
				$pieceData->type = 'curve';
			}
			if (!empty($piece->switch))
			{
				$pieceData->switch = true;
			}
			else
			{
				$pieceData->switch = false;
			}

			$laneCount = count($this->_lanes);

			foreach ($this->_lanes as $index => $offset)
			{
				if ($pieceData->type == 'straight')
				{
					$pieceLength = $piece->length;
				}
				else
				{
					$pieceData->turnDirection = $piece->angle > 0 ? 'right' : 'left';
					$laneRadius = $piece->radius + ($offset * ($pieceData->turnDirection == 'right' ? -1 : 1));

					$pieceLength = abs(($laneRadius * 3.141592654 * 2) / 360 * $piece->angle);

					$crashFactor = $laneRadius / abs($piece->angle);
					$pieceData->lanes[$index]['speedFactor'] = abs($crashFactor);
				}
				$pieceData->lanes[$index]['length'] = $pieceLength;

				if (!empty($piece->switch))
				{
					if ($index != 0)
					{
						$pieceData->lanes[$index]['switchRight'] = sqrt(pow($offset - $this->_lanes[$index - 1], 2) + pow($pieceLength, 2));
					}
					if ($index != $laneCount - 1)
					{
						$pieceData->lanes[$index]['switchLeft'] = sqrt(pow($offset - $this->_lanes[$index + 1], 2) + pow($pieceLength, 2));
					}
				}
			}
			$this->_trackPieces[] = $pieceData;
		}
	}

	public function getSpeed($lane, $pieceIndex)
	{
		if (empty($this->_speedStrategy))
		{
			$this->generateSpeedStrategy();
		}

		// Look ahead this much
		$blockRange = 3;

		// Add the existing track at the end of itself, so we can't overflow
		$blocks = $this->_speedStrategy;
		foreach ($this->_speedStrategy as $piece)
		{
			$blocks[] = $piece;
		}

		$speeds = array();
		for ($i = 0; $i < $blockRange; $i++)
		{
			$speeds[] = $blocks[$i + $pieceIndex][$lane];
		}

		sort($speeds);
		$speed = ($speeds[0] + $speeds[1]) / 2;

		return $speed;
	}

	public function generateSpeedStrategy()
	{
		$speedStrategy = array();
		foreach ($this->_trackPieces as $index => $piece)
		{
			foreach ($this->_lanes as $laneIndex => $lane)
			{
				switch($piece->type)
				{
					case 'curve':
						$throttle = min(1, ($piece->lanes[$laneIndex]['speedFactor'] * 0.1 * self::SPEED_BASE));
						break;
					default:
						$throttle = 1;
				}
				$speedStrategy[$index][$laneIndex] = $throttle;
			}
		}
		$this->_speedStrategy = $speedStrategy;
	}
} 
